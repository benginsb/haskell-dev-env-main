-- file test/WeeklyHaskellOneSpec.hs
module WeeklyHaskellOneSpec where

import Test.Hspec 
import WeeklyHaskellOne

spec :: Spec
spec = do
    describe "removeChar" $ do
        it "removeChar a in apples" $
            removeChar 'a' "apples" `shouldBe` "pples"
        it "removeChar p in apples" $
            removeChar 'p' "apple" `shouldBe` "ale"
    
    describe "removeWhitespace" $ do
        it "removeWhitespace in phrase abc def" $
            removeWhitespace "abc def" `shouldBe` "abcdef"
        it "removeWhitespace in phrase a b\t c\n d e\r f" $
            removeWhitespace "a b\t c\n d e\r f" `shouldBe` "abcdef"

    describe "removePunctuation" $ do
        it "removePunctuation in phrase a,b.c\"d[e]f" $
            removePunctuation "a,b.c\"d[e]f" `shouldBe` "abcdef"

    describe "charsToAscii" $ do
        it "charsToAscii in hello" $
            charsToAscii "hello" `shouldBe` [104, 101, 108, 108, 111]
        it "charsToAscii in test" $
            charsToAscii "test" `shouldBe` [116, 101, 115, 116] 

    describe "asciiToChars" $ do
        it "asciiToChars in [104 101 108 108 111]" $
            asciiToChars [104, 101, 108, 108, 111] `shouldBe` "hello"
        it "asciiToChars" $
            asciiToChars [116, 101, 115, 116] `shouldBe` "test"

    describe "shiftInts" $ do
        it "shiftInts of 15 [1,5,120, 125]" $
            shiftInts 15 [1,5,120, 125] `shouldBe` [16, 20, 7, 12]


    describe "shiftMessage" $ do
        it "shiftMessage of Hello" $
            shiftMessage 4 "Hello" `shouldBe` "Lipps"
        it "removeChar]" $
            shiftMessage 18 "Testing Testing 1 2 3 " `shouldBe` "fw\ENQ\ACK{\NULy2fw\ENQ\ACK{\NULy2C2D2E2"