module DailySix where


-- shorterThan
-- Takes in an int and a string
-- If the length is longer than it is add, else it is not added
shorterThan :: Int -> [String] -> [String]
shorterThan _ [] = []
shorterThan num str = filter (\x -> length x <= num) str

-- removeMultiples
-- Takes in an int and a list of ints
-- Adds the numbers if not divisible by the number
removeMultiples ::  Int -> [Int] -> [Int]
removeMultiples _ [] = []
removeMultiples num list = filter (\x -> x `mod` num /= 0) list

-- onlyJust
-- Takes in Maybe a and poruces a Maybe a
--  If it doesnt equal nothing, then print
onlyJust :: Eq a => [Maybe a] -> [Maybe a]
onlyJust [] = []
onlyJust list = filter (\x -> x /= Nothing) list




