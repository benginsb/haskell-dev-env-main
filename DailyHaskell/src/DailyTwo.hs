-- file src/DailyTwo.hs
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module DailyTwo where

    -- every4th
    -- Take in a string of Integers 
    -- Recursively take in every 4th element
    every4th :: [a] -> [a]
    every4th [] = []
    every4th (_:_:_:x:xs) = x : every4th xs
    every4th _ = []

    -- tupleDotQuotient
    -- add the quotient of the first number divded by the 2nd number
    tupleDotQuotient :: Floating a => [a] -> [a] -> a
    tupleDotQuotient x y = findQuotient x y 0
        where
            findQuotient [] [] add = add
            findQuotient (x:xs) (y:ys) add = findQuotient xs ys (add + x / y)

    -- appendToEach
    -- Reads in a string and a list of Strings
    -- Recursively goes through and adds the strings to the end of each one
    appendToEach :: String -> [String] -> [String]
    appendToEach _ [] = []
    appendToEach n (x:xs) = (x ++ n) : appendToEach n xs


    -- toSetList
    -- check if the element is list
    -- If the element is in the list, do nothing
    -- else add it to the list
    toSetList :: (Eq a) => [a] -> [a]
    toSetList [] = []
    toSetList (x:xs) = toSetList xs ++ rVal
        where
            rVal = if x `elem` xs then [] else [x]