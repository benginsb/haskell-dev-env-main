module DailyNineSpec where

import Test.Hspec 
import DailyNine

spec :: Spec
spec = do
    describe "findSmallest" $ do
        it "outputs findSmallest []" $
            findSmallest [1] `shouldBe` Just 1
        it "outputs findSmallest [12, 13]" $
            findSmallest [12, 13] `shouldBe` Just 12
        it "outputs findSmallest [9,8,7,6,5,4,3,2,1]" $
            findSmallest [9,8,7,6,5,4,3,2,1] `shouldBe` Just 1
    
    describe "allTrue" $ do
        it "outputs findSmallest []" $
            allTrue [] `shouldBe` Nothing
        it "outputs findSmallest [12, 13]" $
            allTrue [True, True, True] `shouldBe` Just True 
        it "outputs findSmallest [9,8,7,6,5,4,3,2,1]" $
            allTrue [True, True, True, False] `shouldBe` Just False 

    describe "countAllVotes" $ do
        it "outputs findSmallest []" $
            countAllVotes [] `shouldBe` (0,0,0)
        it "outputs findSmallest [12, 13]" $
            countAllVotes [Just True, Just True, Just True] `shouldBe` (0,3,0)
        it "outputs countAllVotes [Just True, Just True, Just True, Just False, Just False, Just False, Nothing, Nothing, Nothing]" $
            countAllVotes [Just True, Just True, Just True, Just False, Just False, Just False, Nothing, Nothing, Nothing] `shouldBe` (3,3,3) 

