-- file src/WeeklyHaskellOne.hs
module WeeklyHaskellOne where


-- Consumes a single character and a string. 
-- Produces a new string with all instances of the character removed. You may use foldr. 
-- removeChar
    removeChar :: Char -> String -> String 
    removeChar ch = foldr (\a b -> if a == ch then b else a:b) ""


-- consumes a string. 
-- Produces a new string with all spaces, tabs, new line characters, and carriage returns removed. This function should compose instances of removeChar.
-- removeWhitespace
    removeWhitespace :: String -> String 
    removeWhitespace = rChar1. rChar2 . rChar3 . rChar4
        where
            rChar1 = removeChar ' ' 
            rChar2 = removeChar '\t'
            rChar3 = removeChar '\n' 
            rChar4 = removeChar '\r'



-- consumes a string.
-- Produces a new string with all commas, periods, parentheses, square brackets, and curly brackets removed. 
-- This function should compose instances of removeChar.
-- removePunctuation
    removePunctuation :: String -> String 
    removePunctuation = rChar1 . rChar2 . rChar3 . rChar4 . rChar5
        where
            rChar1 = removeChar ',' 
            rChar2 = removeChar '.'
            rChar3 = removeChar '"' 
            rChar4 = removeChar '['
            rChar5 = removeChar ']'

-- consumes a string. 
-- Produces a new list containing the ASCII values of the characters in the given string. You can use the Haskell fromEnum function defined in the Haskell Prelude.
-- charsToAscii
    charsToAscii :: String -> [Int]
    charsToAscii [] = []
    charsToAscii (x:xs) = fromEnum x: charsToAscii xs


-- consumes a list of integers (which are assumed to be valid ASCII values). 
-- Produces a new list of characters created from the ASCII values. You can use the Haskell toEnum function defined in the Haskell Prelude. 
-- For example, (fromEnum 'a') will return 97, which is the ASCII character code for the lowercase letter a.
-- asciiToChars 
    asciiToChars :: [Int] -> [Char]
    asciiToChars [] = []
    asciiToChars (x:xs) = toEnum x : asciiToChars xs



-- consumes an integer (the shift value) and a list of integers (which are assumed to be valid ASCII values). 
-- Produces a new list of integers where each value in the given list has been increased by the shift value (modulo 128 which is the maximum ASCII value). 
-- For example, shiftInts 1 [2, 4, 6, 127] should produce [3, 5, 7, 0].
-- shiftInts 
    shiftInts :: Int -> [Int] -> [Int]
    shiftInts _ [] = []
    shiftInts n (x:xs) =  (n + x) `mod` 128 : shiftInts n xs



-- consumes an integer (the shift value) and a string (the message). 
-- Produces a new string which is the encrypted message where each character has been shifted by the shift value in the ASCII encoding. 
-- Note that this can also be used to decrypt a message using a negative integer as the shift value. 
-- shiftMessage
    shiftMessage :: Int  -> String -> String 
    shiftMessage x n = asciiToChars $ shiftInts x $ charsToAscii n


