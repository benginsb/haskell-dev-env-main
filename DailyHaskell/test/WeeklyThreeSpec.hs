
module WeeklyThreeSpec where

import Test.Hspec 
import WeeklyThree


spec :: Spec
spec = do
    describe "Vec " $ do
        it "outputs Vec [1.0, 2.0, 3.0, 4.0]" $
            Vec [1.0, 2.0, 3.0, 4.0] `shouldBe` Vec [1.0, 2.0, 3.0, 4.0]
        it "outputs Vec [ 3.0, 4.0, 7.0, 8.9]" $
            Vec [ 3.0, 4.0, 7.0, 8.9] `shouldBe` Vec [ 3.0, 4.0, 7.0, 8.9]
        it "outputs Vec [10.0, 11.0, 12.0, 13.0]" $
            Vec [10.0, 11.0, 12.0, 13.0] `shouldBe` Vec [10.0, 11.0, 12.0, 13.0]
    
    describe "Vec +,-,*,abs,sigma, fromInteger, negate" $ do
        it "outputs (Vec [1, 2, 3] + Vec [3, 2, 1])" $
            (Vec [1, 2, 3] + Vec [3, 2, 1]) `shouldBe` Vec [4.0,4.0,4.0]
        it "outputs firstFunctorLaw [2, 3, 5, 7, 11]" $
             (Vec [123, 456, 789] + Vec [132, 243, 354]) `shouldBe` Vec [255.0,699.0,1143.0]
        it "outputs (Vec [111, 333, 555] + Vec [777, 999, 11,11,11])" $
             (Vec [111, 333, 555] + Vec [777, 999, 11,11,11]) `shouldBe` Vec [888.0,1332.0,566.0]
        
        it "outputs (Vec [1, 2, 3] - Vec [3, 2, 1])" $
            (Vec [1, 2, 3] - Vec [3, 2, 1]) `shouldBe` Vec [-2.0,0.0,2.0]
        it "outputs (Vec [123, 456, 789] - Vec [132, 243, 354]" $
             (Vec [123, 456, 789] - Vec [132, 243, 354]) `shouldBe` Vec [-9.0,213.0,435.0]
        it "outputs (Vec [111, 333, 555] - Vec [777, 999, 11,11,11])" $
             (Vec [111, 333, 555] - Vec [777, 999, 11,11,11]) `shouldBe` Vec [-666.0,-666.0,544.0]

        it "outputs abs (Vec [-2.0,0.0,2.0])" $
            abs (Vec [-2.0,0.0,2.0]) `shouldBe` Vec [2.0,0.0,2.0]
        it "outputs  abs (Vec [-9.0,213.0,-435.0])" $
            abs (Vec [-9.0,213.0,-435.0]) `shouldBe` Vec [9.0,213.0,435.0]
        it "outputs abs (Vec [3.0,-1.0,0.0])" $
            abs (Vec [3.0,-1.0,0.0]) `shouldBe` Vec [3.0,1.0,0.0]    
        
        it "outputs signum  (Vec [-2.0,0.0,2.0])" $
            signum (Vec [-2.0,0.0,2.0]) `shouldBe` Vec [-1,-0,1]
        it "outputs signum  (Vec [-9.0,213.0,-435.0])" $
            signum (Vec [-9.0,213.0,-435.0]) `shouldBe` Vec [-1,1,-1]       
        it "outputs signum (Vec [3.0,0.0,0.0])" $
            signum (Vec [3.0,0.0,0.0]) `shouldBe` Vec [1,0,0]    


        it "outputs fromInteger 5" $
            fromInteger 2 `shouldBe` Vec [2]
        it "outputs fromInteger 34" $
            fromInteger 34 `shouldBe` Vec [34]
        it "outputs fromInteger 99" $
            fromInteger 99 `shouldBe` Vec [99]      
        
        it "outputs negate (Vec [1, 2, 3] <> Vec [3, 2, 1])s" $ do
            negate (Vec [1, 2, 3] <> Vec [3, 2, 1]) `shouldBe`  Vec [-4.0,-4.0,-4.0]
        it "outputs negate (Vec [2, 4, 6] <> Vec [11, 13, 15]) " $ do
            negate (Vec [2, 4, 6] <> Vec [11, 13, 15]) `shouldBe`  Vec [-13.0,-17.0,-21.0]
        it "outputs negate (Vec [6, 7, 8] <> Vec [9, 10, 11]) " $ do
            negate (Vec [6, 7, 8] <> Vec [9, 10, 11]) `shouldBe`  Vec [-15.0,-17.0,-19.0]

    describe "Vec as a magnitude" $ do
        it "outputs magnitude (Vec [1, 8])" $
            magnitude (Vec [1, 8]) `shouldBe` 8.06225774829855
        it "calculates magnitude (Vec [6, 4])" $
            magnitude (Vec [6, 4]) `shouldBe` 7.211102550927978
        it "calculates magnitude (Vec [4, 5])" $
            magnitude (Vec [4, 5]) `shouldBe` 6.4031242374328485
    
    describe "Vec as a Semigroup" $ do
        it "outputs (Vec [4, 5] <> Vec [6, 7])" $
            (Vec [4, 5] <> Vec [6, 7]) `shouldBe` Vec [10, 12]
        it "outputs (Vec [8, 9] <> Vec [10, 11])" $
            (Vec [8, 9] <> Vec [10, 11]) `shouldBe` Vec [18, 20]
        it "outputs (Vec [12, 13] <> Vec [14, 15])" $
            (Vec [12, 13] <> Vec [14, 15]) `shouldBe`Vec [26.0,28.0]
    
    describe "Vec as a Monoid" $ do
        it "outputs  mempty" $
            mempty `shouldBe` Vec [0]