{-# OPTIONS_GHC -Wno-type-defaults #-}
module TriTreeSpec where


import Test.Hspec 
import TriTree

spec :: Spec
spec = do

    let tree = Node 5 7 (Leaf 2) (Node 6 6 (Leaf 5) Empty Empty) (Node 8 9 Empty Empty Empty)
    let tree'  = Node 3 8  (Node 1 2 Empty (Leaf 2) Empty) (Node 4 7  (Leaf 5) Empty (Leaf 6)) (Node 9 10 Empty Empty Empty)
    describe "search" $ do
        it "outputs search 4" $
            search 4 tree `shouldBe` False
        it "outputs search 5" $
            search 5 tree `shouldBe` True
        it "outputs search 15" $
            search 15 tree `shouldBe` False

    describe "insert" $ do
        it "outputs insert 4" $
            insert 4 tree `shouldBe` Node 5 7 (Node 2 4 Empty Empty Empty) (Node 6 6 (Leaf 5) Empty Empty) (Node 8 9 Empty Empty Empty)
        it "outputs insert 5" $
            insert 5 tree `shouldBe` Node 5 7 (Leaf 2) (Node 6 6 (Node 5 5 Empty Empty Empty) Empty Empty) (Node 8 9 Empty Empty Empty)
        it "outputs insert 15" $
            insert 15 tree' `shouldBe` Node 3 8 (Node 1 2 Empty (Leaf 2) Empty) (Node 4 7 (Leaf 5) Empty (Leaf 6)) (Node 9 10 Empty Empty (Leaf 15))


    describe "insertList" $ do
        it "outputs insertList [1,2,3,4,5]" $
            insertList [1,2,3,4,5] `shouldBe` Node 4 5 (Node 2 3 (Leaf 1) Empty Empty) Empty Empty
        it "outputs insertList insertList [2,3,4,6,8,12,14,15,17,18,19,20]" $
            insertList [2,3,4,6,8,12,14,15,17,18,19,20] `shouldBe` Node 19 20 (Node 17 18 (Node 14 15 (Node 8 12 (Node 4 6 (Node 2 3 Empty Empty Empty) Empty Empty) Empty Empty) Empty Empty) Empty Empty) Empty Empty
        it "outputs insertList insertList [20,15,14,6,3,2,8]" $
            insertList [20,15,14,6,3,2,8] `shouldBe` Node 2 8 Empty (Node 3 6 Empty Empty Empty) (Node 14 15 Empty Empty (Leaf 20))

    describe "identical " $ do
        it "outputs identical tree and tree" $
            identical tree tree`shouldBe` True
        it "outputs identical  tree and tree'" $
            identical tree' tree `shouldBe` False
        it "outputs identical tree and tree'" $
            identical tree' tree' `shouldBe` True

    describe "treeMap" $ do
        it "outputs treeMap tree x+2" $
            treeMap (\x-> x + 2) tree `shouldBe` Node 7 9 (Leaf 4) (Node 8 8 (Leaf 7) Empty Empty) (Node 10 11 Empty Empty Empty)
        it "outputs treeMap  tree' x+3" $
            treeMap (\x-> x + 3) tree' `shouldBe` Node 6 11 (Node 4 5 Empty (Leaf 5) Empty) (Node 7 10 (Leaf 8) Empty (Leaf 9)) (Node 12 13 Empty Empty Empty)
        it "outputs treeMap tree x^2'" $
            treeMap (\x-> x ^2) tree `shouldBe` Node 25 49 (Leaf 4) (Node 36 36 (Leaf 25) Empty Empty) (Node 64 81 Empty Empty Empty)


    describe "treeFoldPreOrder" $ do
        it "outputs treeFoldPreOrder (x y -> x + y) 0" $
            treeFoldPreOrder (\x y -> x + y) 0 tree `shouldBe` 48
        it "outputs treeFoldPreOrder (x y -> x + x * y) 1" $
            treeFoldPreOrder (\x y -> x + x * y) 1 tree `shouldBe` 3810240
        it "outputs treeFoldPreOrder (x y -> x +  y^2) 2"  $
            treeFoldPreOrder (\x y -> x +  y^2) 2 tree `shouldBe` 322

    describe "treeFoldInOrder" $ do
        it "outputs  treeFoldInOrder (x y -> x + y)" $
            treeFoldInOrder (\x y -> x + y) 0 tree `shouldBe` 48
        it "outputs treeFoldInOrder (x y -> x + x * y) 1" $
            treeFoldInOrder (\x y -> x + x * y) 1 tree `shouldBe` 3810240
        it "outputs treeFoldInOrder (x y -> x +  y^2) 2"  $
            treeFoldInOrder (\x y -> x +  y) 2 tree `shouldBe` 50

    describe "treeFoldPostOrder" $ do
        it "outputs treeFoldPostOrder (x y -> x + y) 0" $
            treeFoldPostOrder (\x y -> x + y) 0 tree `shouldBe` 48
        it "outputs treeFoldPostOrder (x y -> x + x * y) 1" $
            treeFoldPostOrder (\x y -> x + x * y) 1 tree `shouldBe` 3810240
        it "outputs treeFoldPostOrder (x y -> x +  y^2) 2"  $
            treeFoldPostOrder (\x y -> x +  y) 2 tree `shouldBe` 50




