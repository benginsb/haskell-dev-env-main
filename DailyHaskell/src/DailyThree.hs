-- file test/DailyThree.hs
module DailyThree where
-- removeAllExcept :: Eq a => a -> [a] -> [a]
-- Take in the array and element
-- returns the rest of the array plus the current element (though recursion) if they are equal else only returns rest of array

removeAllExcept :: Eq a => a -> [a] -> [a]
removeAllExcept _ [] = []
removeAllExcept n (x:xs) = if n == x then x:removeAllExcept n xs else removeAllExcept n xs


-- countOccurrences :: Eq a => a -> [a] -> Int.
-- take in the array of elements and number
-- do an if statment where 1 is added if the number is equal to the current number
countOccurrences :: Eq a => a -> [a] -> Int
countOccurrences _ [] = 0
countOccurrences n (x:xs) = if n == x then 1 + countOccurrences n xs else countOccurrences n xs


-- substitute :: Eq a => a -> a -> [a] -> [a]
-- Take in array and elements
-- If equal replace, else leave the same
substitute :: Eq a => a -> a -> [a] -> [a]
substitute _ _ [] = []
substitute a b (x:xs) =  subReturn : substitute a b xs
    where
        subReturn = if x == a then b else x 