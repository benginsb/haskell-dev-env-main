module DailyNine where

-- findSmallest
-- base cases of only 1 element and no elements
-- goes over the list reursively and finds the smallest element, putting it in front
findSmallest :: (Ord a) => [a] -> Maybe a
findSmallest [] = Nothing 
findSmallest [x] = Just x 
findSmallest (min:x:xs) = findSmallest((if min <= x then min else x):xs)


-- allTrue
-- use the "and" function to check if all elements are true, if true, thne return true else return false
allTrue :: [Bool] -> Maybe Bool
allTrue [] = Nothing 
allTrue x = if and x then Just True else Just False


-- countAllVotes
-- base case of (0,0,0)
-- use a while to add to first element if Nothing, second elemenet if Just yes
--      and third element if just no
--  then interates over xs
countAllVotes :: [Maybe Bool] -> (Integer, Integer, Integer)
countAllVotes [] = (0,0,0)
countAllVotes (x:xs) = (nd, yes, no)
                            where    
                                (nd, yes, no) = countAllVotes' (x:xs)
                                countAllVotes' [] = (0,0,0)
                                countAllVotes' (x:xs)
                                    | x == Just True = (nd, yes+1, no)
                                    | x == Just False = (nd, yes, no+1)
                                    | otherwise = (nd+1, yes, no)
                                         where (nd, yes, no) = countAllVotes' xs