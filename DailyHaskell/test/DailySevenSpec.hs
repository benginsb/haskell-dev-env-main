module DailySevenSpec where

import Test.Hspec 
import DailySeven

spec :: Spec
spec = do
    describe "findLongest" $ do
        it "outputs findLongest 5 [1,2,3,4]" $
            findLongest ["apple","bananas","pear","watermellon"] `shouldBe` "apple"
        it "outputs findLongest 2 [hi,bye,hello,Goodbye]" $
            findLongest ["hi","bye","hello","Goodbye"] `shouldBe` "hello"
        it "outputs findLongest  []" $
            findLongest [] `shouldBe` ""

    describe "anyLarger" $ do
        it "outputs anyLarger 5 [1,2,3,4]" $
            anyLarger 5 [1,2,3,4]`shouldBe` False
        it "outputs anyLarger anyLarger 5 [1,3,5,2]" $
            anyLarger 5 [1,3,5,2]`shouldBe` True
        it "outputs anyLarger 5 []" $
            anyLarger 5 [] `shouldBe` False

    describe "allNames" $ do
        it "outputs allNames [(kermit, the frog), (bugs, bunny)]" $
            allNames [("kermit", "the frog"), ("bugs", "bunny")] `shouldBe` "kermit the frog, bugs bunny"
        it "outputs allNames anyLarger [(Hello,Goodbye]" $
            allNames [("Hello","Goodbye")] `shouldBe` "Hello Goodbye"
        it "outputs allNames  []" $
            allNames [] `shouldBe` []
