module EvalSpec where


import Test.Hspec
import Parser
import Expr
import MiniRacketParser

import Eval
import Error

type ParseResult = Either ErrorT (Expr, String)

spec :: Spec
spec = do
    describe "eval expressions" $ do
        it "evaluates number: 1235" $ 
            evalStr "1235" `shouldBe` Right (IntVal 1235)
        it "evaluates negative numbers: -12235" $
            evalStr "-12235" `shouldBe` Right (IntVal (-12235))
        it "evaluates true" $
            evalStr "true" `shouldBe` Right (BoolVal True)
        it "evaluates false" $
            evalStr "false" `shouldBe` Right (BoolVal False)
        it "evaluates false" $
            evalStr "false" `shouldBe` Right (BoolVal False)
        it "evaluates evalStr (cons 4 5)" $
            evalStr "(cons 4 5)" `shouldBe` Right (PairVal (IntVal 4,IntVal 5))
        it "evaluates evalStr (cons 10 100)" $
            evalStr "(cons 10 100)" `shouldBe` Right (PairVal (IntVal 10,IntVal 100))
        it "evaluates evalStr (+ 1 2)" $
            evalStr "(+ 1 2)" `shouldBe` Right (IntVal 3)
        it "evaluatess evalStr (- 1 2)" $
            evalStr "(- 1 2)" `shouldBe` Right (IntVal (-1))
        it "evaluates evalStr (* 3 2)" $
            evalStr "(* 3 2)" `shouldBe` Right (IntVal 6)
        it "evaluates evalStr (/ 3 3)" $
            evalStr "(/ 3 3)" `shouldBe` Right (IntVal 1)
        it "evaluates evalStr (< 1 2)" $
            evalStr "(< 1 2)" `shouldBe` Right (BoolVal True)
        it "evaluates evalStr (< 2 1)" $
            evalStr "(< 2 1)" `shouldBe` Right (BoolVal False)
        it "evaluates evalStr (equals? 2 1)" $
            evalStr "(equals? 2 1)" `shouldBe` Right (BoolVal False)
        it "evaluates evalStr (equals? 2 2)" $
            evalStr "(equals? 2 2)" `shouldBe` Right (BoolVal True)