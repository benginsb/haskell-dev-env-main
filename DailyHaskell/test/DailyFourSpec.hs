-- file test/DailyFourSpec.hs
{-# OPTIONS_GHC -Wno-type-defaults #-}
module DailyFourSpec where

import Test.Hspec 
import DailyFour

spec :: Spec
spec = do
    describe "zip3Lists" $ do
        it "outputs zip3Lists [1, 2, 3] ['a', 'b', 'c'][4, 5, 6]" $
            zip3Lists [1, 2, 3] ['a', 'b', 'c'][4, 5, 6] `shouldBe` [(1, 'a', 4), (2, 'b', 5), (3, 'c', 6)]
        it "outputs zip3Lists of [1, 2, 3,4] [5, 6, 7,8][9, 10, 11,12]" $
            zip3Lists [1, 2, 3,4] [5, 6, 7,8] [9, 10, 11,12] `shouldBe` [(1,5,9),(2,6,10),(3,7,11),(4,8,12)]
        it "outputs zip3Lists of [True, False] [1, 2] [')','*']" $
            zip3Lists [True, False] [1, 2] [')','*'] `shouldBe` [(True,1,')'),(False,2,'*')]

    
    describe "unzipTriples" $ do
        it "outputs unzipTriples [(1, 'a', 4), (2, 'b', 5), (3, 'c', 6)]" $
            unzipTriples [(1, 'a', 4), (2, 'b', 5), (3, 'c', 6)] `shouldBe` ([1, 2, 3], ['a', 'b', 'c'],[4, 5, 6])
        it "outputs unzipTriples of [ (1,2,3), (4, 5, 6), (7, 8, 9) ]]" $
            unzipTriples [ (1,2,3), (4, 5, 6), (7, 8, 9) ] `shouldBe` ( [1,4,7], [2, 5, 8], [3, 6, 9] )
        it "outputs unzipTriplesof []" $
            unzipTriples [('a','b','c'), ('d','e','f'), ('g','h','i')] `shouldBe` (['a','d','g'],['b','e','h'],['c','f','i'])
            

    describe "mergeSorted3" $ do
        it "outputs mergeSorted3 [2, 3, 5] [1, 8] [-1, 0, 4, 10]" $
            mergeSorted3 [2, 3, 5] [1, 8] [-1, 0, 4, 10] `shouldBe` [-1, 0, 1, 2, 3, 4, 5, 8, 10]
        it "outputs mergeSorted3 of [1, 2, 3,4] [5, 6, 7,8] [9, 10, 11,12]" $
            mergeSorted3 [1, 2, 3,4] [5, 6, 7,8] [9, 10, 11,12] `shouldBe` [1,2,3,4,5,6,7,8,9,10,11,12]
        it "outputs mergeSorted3 of [1,2,6,8] [5,6,17] [3,15]" $
            mergeSorted3 [1,2,6,8] [5,6,17] [3,15] `shouldBe` [1,2,3,5,6,6,8,15,17]