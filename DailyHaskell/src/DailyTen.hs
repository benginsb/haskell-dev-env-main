module DailyTen where


-- firstFunctorLaw 
-- takes in the functor and a and sees if its equilivant to the first element in the list
firstFunctorLaw :: (Eq (f a), Functor f) => f a -> Bool
firstFunctorLaw fun = fmap id fun == fun

-- secondFunctorLaw
-- Takes in 2 paramaters and a fucntor and equal if the second paramater is equal to the functor
secondFunctorLaw :: (Eq (f c), Functor f) => (b -> c) -> (a -> b) -> f a -> Bool
secondFunctorLaw f g fun = fmap (f . g) fun == fmap f (fmap g fun)