module DailyTwelve where


-- onlyNothing
onlyNothing :: (a -> Maybe b ) -> [a] -> Bool
onlyNothing fun [] = True 
onlyNothing fun (x:xs) = case fun x of
    Just _ -> False 
    Nothing -> onlyNothing fun xs



-- firstAnswer
-- if list is empty return nothing
    -- remove element in list until empty to pattern match
    -- return if just pops up
firstAnswer :: ( a -> Maybe b ) -> [a] -> Maybe b
firstAnswer fun [] = Nothing
firstAnswer fun (x:xs) = case fun x of
    Just v -> Just v
    Nothing -> firstAnswer fun xs


-- allAnswers
-- basecase returns the list 
-- if nothing, then return nothing
    -- else add add elements to list
allAnswers :: (a -> Maybe [b]) -> [a] -> Maybe [b]
allAnswers fun [] = Just []
allAnswers fun (x:xs) = case fun x of
    Nothing -> Nothing 
    Just v -> case allAnswers fun xs of
        Nothing -> Nothing
        Just vs -> Just (v ++ vs)