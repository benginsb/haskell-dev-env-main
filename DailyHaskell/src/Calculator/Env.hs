module Calculator.Env where

-- emptyEnv
--   Produce an empty environment with no defined variables
emptyEnv :: [(a,b)]
emptyEnv = []

-- extendEnv
--   Consume a (variable, value) and an Environment
--   Produce a new Environment with the new (variable, value) in it
extendEnv :: (String, b) -> [(String, b)] -> [(String, b)]
extendEnv (name, value) env = (name, value) : env

-- applyEnv
--    Consume a name and an environment
--    Produce the associated value of the name variable
applyEnv :: String -> [(String,b)] -> b
applyEnv _ [] = error "Variable undefined"
applyEnv name ((key,value):kvs) = if name == key
                                     then value 
                                     else applyEnv name kvs

-- Functionalized Version of the Environment Interface
emptyEnv' :: String -> a
emptyEnv' = \name -> error "Variable is undefined"

extendEnv' :: (String, p) -> (String -> p) -> String -> p
extendEnv' (name, value) fenv = \key -> if name == key
                                           then value
                                           else fenv name

applyEnv' :: String -> (String -> p) -> p
applyEnv' name fenv = fenv name 