module Calculator.BoolLexer where
import Data.Char 
data Token = TokenBoolConst String|
               TokenName String |
               TokenAnd | 
               TokenOr |
               TokenNot |
               TokenEqual |
               TokenKeyword String |
               TokenEnd 
               deriving (Show)
-- lexString
--  Consume a String that contains the Program Text for the
--     Boolean Calculator
--  Produce a List of Tokens
lexString :: String -> [Token]
lexString [] = []
lexString ('=':rem) = TokenEqual : (lexString rem)
lexString ('*':rem) = TokenAnd : (lexString rem)
lexString ('+':rem) = TokenOr : (lexString rem)
lexString ('!':rem) = TokenNot : (lexString rem)
lexString str = case str of
                     (c:cs) | isSpace c -> lexString cs 
                     (c:cs) | isAlpha c -> getName str 
                where
                    getName str = lexName i : lexString cs 
                       where (i, cs) = span isAlpha str 
                    lexName i = if isReservedWord i 
                                   then (TokenKeyword i)
                                   else if isBoolConst i 
                                            then (TokenBoolConst i)
                                            else (TokenName i)
                    isReservedWord w = elem w ["Let", "In", "Lambda","Call"]
                    isBoolConst w = elem w ["True", "False"]
                    
