module TinyParser where

--    A Parser for things
-- Is a function from strings
--     To lists of pairs
--   Of things and strings

import TinyDefinitions
import TinyLexer

import MonadicParserLibrary
import Control.Applicative 

import Debug.Trace

-- parseString
--   Consume a string containing a Tiny Language program 
--   Produce a structure representing the parse tree of the program 
-- parseString :: String -> [(ParseTree,String)]
parseString :: String -> ParseTree 
parseString program = 
    trace ("parsing '" ++ program ++ "'") $
    let [(tree,remainingChars)] = parse expressionParser program
                          in
                             case remainingChars of 
                                  "" -> tree
                                  _ -> error ("Parse Error: " ++ remainingChars)

-- expressionParser
--   Produce a parser for an expression in the Tiny Language
--     The parser will produce a ParseTree representing the 
--         program 

expressionParser :: Parser ParseTree  
expressionParser = do iCombinatorParser
                    <|>
                   do kCombinatorParser
                    <|>
                   do sCombinatorParser
                    <|>
                   do idParser
                    <|>
                   do applyTwoParser
                    <|>
                   do applyThreeParser
                    <|>
                   do applyFourParser

idParser :: Parser ParseTree 
idParser = do name <- idName 
              return (IdNode name)

iCombinatorParser :: Parser ParseTree 
iCombinatorParser = do i <- iCombinatorKeyword 
                       return INode

kCombinatorParser :: Parser ParseTree 
kCombinatorParser = do k <- kCombinatorKeyword 
                       return KNode

sCombinatorParser :: Parser ParseTree 
sCombinatorParser = do s <- sCombinatorKeyword 
                       return SNode

applyTwoParser :: Parser ParseTree  
applyTwoParser = do leftParenthesis
                    fun <- expressionParser
                    space
                    parameter <- expressionParser
                    rightParenthesis
                    return (ApplyTwoNode fun parameter)

applyThreeParser :: Parser ParseTree  
applyThreeParser = do leftParenthesis
                      fun <- expressionParser
                      space
                      paramOne <- expressionParser
                      space 
                      paramTwo <- expressionParser
                      rightParenthesis
                      return (ApplyThreeNode fun paramOne paramTwo)

applyFourParser :: Parser ParseTree  
applyFourParser = do leftParenthesis
                     fun <- expressionParser
                     space
                     paramOne <- expressionParser
                     space 
                     paramTwo <- expressionParser
                     space
                     paramThree <- expressionParser
                     rightParenthesis
                     return (ApplyFourNode fun paramOne paramTwo paramThree)

