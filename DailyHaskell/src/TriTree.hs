-- file src/TriTree.hs
module TriTree where 

-- Creates the TriTree data structure
data TriTree a = Empty | 
                 Leaf a | 
                 Node a a (TriTree a) (TriTree a) (TriTree a) 
                 deriving (Eq,Show)

-- search
-- Checks the base cases to see if the cur val is eqal to a
-- Traverese down and check if all of node and leaf nodes until a match
-- is found or at bottom.
search :: (Ord a) => a -> TriTree a -> Bool 
search _ Empty = False
search val (Leaf e) = val == e
search val (Node e f left mid right)
    | e == val = True 
    | f == val = True
    | val < e = search val left
    | val > f = search val right
    | otherwise = search val mid


-- Insert
-- Checks if the code is Empty, then adds a leaf val
-- if there is no node: check if if its greater or less, then add accordingly
-- If there is a node, ceck for is less, inbetween, or greaor than e and f and then add a new Node
insert :: Ord a => a -> TriTree a -> TriTree a
insert val Empty = Leaf val
insert val (Leaf e) = if val < e 
                    then Node val e Empty Empty Empty
                    else Node e val Empty Empty Empty
insert val (Node e f left mid right) 
    | val < e = Node e f (insert val left) mid right
    | val > f = Node e f left mid (insert val right)
    | otherwise = Node e f left (insert val mid) right


-- insertList
-- recirevelu call over the insert fuction with foldr and the tree emptya
insertList :: (Ord a) => [a] -> TriTree a
insertList [] = Empty
insertList a = foldr insert Empty a

-- identical
-- checks all of the base cases to see if there are any differences in two different trees
-- Calls the two differnt trees ands checks if there are any differences recurevly.
identical :: Ord a => TriTree a -> TriTree a -> Bool 
identical Empty Empty = True 
identical (Leaf x) (Leaf y) = x == y 
identical (Leaf _) Empty = False
identical Empty (Leaf _) = False
identical Empty  Node {} = False 
identical Node {} Empty  = False 
identical (Leaf _) Node {} = False 
identical Node {} (Leaf _) = False 
identical (Node a0 b0 left0 mid0 right0) (Node a1 b1 left1 mid1 right1)
    | a0 /= a1 = False
    | b0 /= b1 = False
    | left0 /= left1 = False
    | mid0 /= mid1 = False
    | right0 /= right1 = False
    | otherwise = True


-- treeMap
-- Checks base csae to see if tree is empty
--  recureively applives the lambda function to all elemts of the tree
treeMap :: (Ord a,Ord b) => (a->b) -> TriTree a ->TriTree b
treeMap _ Empty = Empty
treeMap a (Leaf c) = Leaf (a c)
treeMap f (Node a b left mid right) = Node (f a)(f b)(treeMap f left)(treeMap f mid)(treeMap f right)

-- treeFoldPreOrder
-- checks base cases to see if tree is empty
-- fold the tree in order: x y left mid right
treeFoldPreOrder :: Ord a => (a -> a -> a) -> a -> TriTree a -> a
treeFoldPreOrder _ a Empty = a
treeFoldPreOrder f a (Leaf x) = f a x
treeFoldPreOrder f a (Node x y left mid right) = let a' = f a x 
                                                    in let a'' = f a' y
                                                        in let a''' = treeFoldPreOrder f a'' left
                                                            in let a'''' = treeFoldPreOrder f a''' mid
                                                                in treeFoldPreOrder f a'''' right

-- treeFoldInOrder
-- checks base cases to see if tree is empty
-- fold the tree in order: left x mid y right
treeFoldInOrder :: Ord a => (a -> a -> a) -> a -> TriTree a -> a
treeFoldInOrder _ a Empty = a
treeFoldInOrder f a (Leaf x) = f a x
treeFoldInOrder f a (Node x y left mid right) = let a' = treeFoldInOrder f a left
                                                    in let a'' = f a' x 
                                                        in let a''' = treeFoldInOrder f a'' mid
                                                            in let a'''' = f a''' y
                                                                in treeFoldInOrder f a'''' right


-- treeFoldPostOrder
-- checks base cases to see if tree is empty
-- fold the tree in order: left mid right x y
treeFoldPostOrder :: Ord a => (a -> a -> a) -> a -> TriTree a -> a
treeFoldPostOrder _ a Empty = a
treeFoldPostOrder f a (Leaf x) = f a x
treeFoldPostOrder f a (Node x y left mid right) = let a' = treeFoldPostOrder f a left
                                                    in let a'' = treeFoldPostOrder f a' mid
                                                        in let a''' = treeFoldPostOrder f a'' right
                                                            in let a'''' = f a''' x 
                                                                in f a'''' y

