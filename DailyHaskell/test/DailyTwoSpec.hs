-- file test/DailyTwoSpec.hs
module DailyTwoSpec where

import Test.Hspec 
import DailyTwo

spec :: Spec
spec = do
    describe "every4th" $ do
        it "outputs every4th number of [1,2,3,4,5]" $
            every4th [1,2,3,4,5] `shouldBe` [4]
        it "outputs every4th number of [1,2,3,4,5, 6, 7, 8]" $
            every4th [1,2,3,4,5,6,7,8] `shouldBe` [4, 8]
    
    describe "tupleDotQuotient" $ do
        it " find the tdq of [1,2,3] and [4,5,6]" $
            tupleDotQuotient [1,2,3] [4,5,6] `shouldBe` 1.15
        it " find the tdq of [4,4,4] and [6,7,8]" $
            tupleDotQuotient [4,4,4][6,7,8] `shouldBe` 1.7380952380952381
    
    describe "appendToEach" $ do
        it "outputes appendToEach   !!! [ Hello, Goodbye]" $
            appendToEach "!!!" [ "Hello", "Goodbye" ] `shouldBe` [ "Hello!!!", "Goodbye!!!" ]
        it "outputes appendToEach   !!! [los, cat, fev, giv]" $
            appendToEach "er" [ "los", "cat", "fev", "giv" ] `shouldBe` [ "loser", "cater", "fever", "giver" ]
    
    describe "toSetList" $ do
        it "toSetList of [1,2,3,4,4,3]" $
            toSetList [1,2,3,4,4,3] `shouldBe` [3,4,2,1]
        it "toSetList  [1,5,7,9,2,5]" $
            toSetList [1,5,7,9,2,5] `shouldBe` [5,2,9,7,1]