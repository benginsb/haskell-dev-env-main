module TinyLexer where

import Control.Applicative 
import MonadicParserLibrary 

import TinyDefinitions

-- Build monadic parsers for each of the words in the Tiny
--   Language 

iCombinatorKeyword :: Parser String 
iCombinatorKeyword = symbol "I"

kCombinatorKeyword :: Parser String
kCombinatorKeyword = symbol "K"

sCombinatorKeyword :: Parser String 
sCombinatorKeyword = symbol "S"

idName :: Parser String 
idName = ident

leftParenthesis :: Parser String
leftParenthesis = symbol "("

rightParenthesis :: Parser String
rightParenthesis = symbol ")"
