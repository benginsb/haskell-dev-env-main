module ListsAndTrees where
    data MyList a = Empty |
                    Node a (MyList a)
                    deriving (Show)
                    
    myLength :: (MyList a) -> Integer
    myLength Empty = 0
    myLength (Node _ rest) = 1 + myLength rest
    
    addToFront :: (a -> (MyList a) -> MyList a)
    addToFront v Empty = Node v Empty
    addToFront val (Node e rest) = (Node val (Node e rest))
    
    --applyF
    -- Applies some function f to every element in the list
    applyF :: (a -> b) -> MyList a -> MyList b
    applyF _ Empty = Empty
    applyF f (Node e rest) = Node (f e) (applyF f rest)

-- binary tree
    data (BinaryTree a) = TreeEmpty |
                          Leaf a |
                          TreeNode a (BinaryTree a) (BinaryTree a)
                          deriving (Show)
    
    add :: a -> BinaryTree a -> BinaryTree a
    add val TreeEmpty = Leaf val
    add val (Leaf e) = TreeNode val (Leaf e) TreeEmpty
    add val (TreeNode e left right) = TreeNode val (TreeNode e left right) TreeEmpty

    search :: (Eq a) => a -> BinaryTree a -> Bool
    search _ TreeEmpty = False
    search val (Leaf e) = val == e
    search val (TreeNode e left right) = val == e || search val left  || search val right 

    treeApplyF :: (a->b) -> BinaryTree a -> BinaryTree b
    treeApplyF _ TreeEmpty = TreeEmpty
    treeApplyF f (Leaf e) = Leaf (f e)
    treeApplyF f (TreeNode e left right) = TreeNode (f e) (treeApplyF f left) (treeApplyF f right)