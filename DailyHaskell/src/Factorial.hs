module Factorial (factOne, factTwo, factorial) where
    -- Factorial
    --  First version
    --  Consume a number n
    -- Produce a number which is the Factorial of n
    factOne :: Integer -> Integer
    factOne n = if n==0
                then 1
                else n*factOne(n-1)
    factTwo :: Integer -> Integer
    factTwo 0 = 1
    factTwo n = n * factTwo(n-1)

    factorial :: Integer -> Integer
    factorial = factOne