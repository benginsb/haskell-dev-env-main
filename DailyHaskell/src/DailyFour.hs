{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module DailyFour where

-- zip3Lists
-- Takes in 3 lists as tuples 
-- Recursively go though the list and add a new (x,y,z) to each one
zip3Lists :: Ord a => [a] -> [a1] -> [a2] -> [(a,a1,a2)]
zip3Lists [] [] [] = []
zip3Lists(x:xs) (y:ys) (z:zs) = (x,y,z) : zip3Lists xs ys zs


-- unzipTriples
-- Iterate over the list
-- Set the x xs y ys and z zs to the a as

unzipTriples :: [(a, b, c)] -> ([a], [b], [c])
unzipTriples [] = ([], [], [])
unzipTriples (a : as) = let (x, y, z) = a in let (xs, ys, zs) = unzipTriples as
                                                in
                                                  (x: xs, y:ys, z:zs)

-- mergeSorted3 :: Ord a => [a] -> [a] -> [a] -> [a]
-- 6 different base cases 
-- compare the elemets of all 3 lists
-- push the lowest one
mergeSorted3 :: Ord a => [a] -> [a] -> [a] -> [a]
mergeSorted3 [] [] [] = []
mergeSorted3 x [] [] = x
mergeSorted3 [] x [] = x
mergeSorted3 [] [] x  = x
mergeSorted3 (x:xs) (y:ys) [] = if x < y
                        then x: mergeSorted3 xs (y:ys) []
                        else y: mergeSorted3 (x:xs) ys []
mergeSorted3 (x:xs) [] (y:ys)  = if x < y
                        then x: mergeSorted3 xs (y:ys) []
                        else y: mergeSorted3 (x:xs) ys []
mergeSorted3 [] (x:xs)  (y:ys)  = if x < y
                        then x: mergeSorted3 xs (y:ys) []
                        else y: mergeSorted3 (x:xs) ys []
mergeSorted3 (x : xs) (y : ys) (z : zs)
  | x <= y && x <= z = x : mergeSorted3 xs (y : ys) (z : zs)
  | y <= x && y <= z = y : mergeSorted3 (x : xs) ys (z : zs)
  | otherwise = z : mergeSorted3 (x : xs) (y : ys) zs


