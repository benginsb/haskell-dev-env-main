-- file test/DailyOne.hs
module DailyOne where
    -- Takes in 4 integers and restuns and Int
    quadratic :: Integer  -> Integer  -> Integer  -> Integer -> Integer    
    quadratic  a b c x = a + b*x + c*(x * x)

    -- Takes in a int and a tuple and returns a tuple
    scaleVector :: Int -> (Int, Int) -> (Int, Int)
    scaleVector a (x, y) = (a*x, a*y)

    -- Takes in 2 3-tuple and returns and Int
    tripleDistance :: Floating a => (a, a, a) -> (a, a, a) -> a
    tripleDistance (x1, y1, z1) (x2,y2,z2) =  sqrt ((x2 -x1)^2 + (y2-y1)^2 + (z2-z1)^2)