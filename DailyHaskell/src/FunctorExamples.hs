module FunctorExamples where 
  findSmallest :: [Integer] -> Maybe Integer 
  findSmallest [] = Nothing
  findSmallest [x] = Just x 
  findSmallest (x:xs) = let result = findSmallest xs 
                          in 
                            if Just x < findSmallest xs 
                              then Just x 
                              else result 



  search :: (Eq a, Ord a) => a -> BinTree a -> Maybe Bool
  search _ EmptyTree = Nothing 
  search val (Leaf e) = Just (val == e)
  search val (Node e left right) = if val == e 
                                         then Just True 
                                         else if val < e 
                                              then (search val left)
                                              else (search val right)

  -- The idea of "mapping" over a "container" or "structure" is called
  --   a Functor. The name Functor is from Category Theory in Maths
  -- map :: (a -> b) -> [a] -> [b]

  inc :: [Integer] -> [Integer]
  inc [] = []
  inc (n:ns) = n + 1 : inc ns 

  sqr :: [Integer] -> [Integer]
  sqr [] = []
  sqr (n:ns) = n*n : sqr ns

  -- Possible definition of the map function
  -- map f [] = []
  -- map f (n:ns) = f n : map f ns 

  inc' :: [Integer] -> [Integer]
  inc' = map (+1) 
  sqr' :: [Integer] -> [Integer]
  sqr' = map (^2)

  inc'' :: Functor t => t Integer -> t Integer 
  inc'' = fmap (+1)   

  inc''' :: Functor t => t (Integer,Integer) -> t (Integer, Integer)
  inc''' = fmap (\(a,b) -> (a+1, b+1))

  data BinTree a = EmptyTree | Leaf a | Node a (BinTree a) (BinTree a)
                      deriving( Show )

  instance Functor BinTree where
    -- fmap :: (a->b) -> BinTree a -> BinTree b
    fmap _ EmptyTree = EmptyTree
    fmap g (Leaf val) = Leaf (g val) 
    fmap g (Node val left right) = Node (g val) (fmap g left) (fmap g right)