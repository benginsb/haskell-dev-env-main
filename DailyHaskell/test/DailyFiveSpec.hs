-- file test/DailyHaskellFiveSpec.hs
module DailyFiveSpec where

import Test.Hspec 
import DailyFive

spec :: Spec
spec = do
  describe "multPairs" $ do
    it "multPairs  [(1,2),(3,4),(5,6)]" $
      multPairs [(1,2),(3,4),(5,6)] `shouldBe` [2,12,30]
  describe "multPairs" $ do
    it "multPairs []" $
      multPairs [] `shouldBe` []
  describe "multPairs " $ do
    it "multPairs of [(30,4),(20,5),(20,8)]" $
      multPairs [(3,4),(20,5),(20,8)] `shouldBe` [12,100,160]
  
  describe "squareList" $ do
    it "squareList  [1,2,3]" $
      squareList [1,2,3] `shouldBe` [(1,1),(2,4),(3,9)]
  describe "squareList" $ do
    it "squareList []" $
      squareList [] `shouldBe` []
  describe "squareList " $ do
    it "squareList of [4,5,8]" $
      squareList [4,5,8] `shouldBe` [(4,16),(5,25),(8,64)]
    
  describe "findLowercase" $ do
    it "findLowercase [\"Hello\"]" $
      findLowercase ["Hello"] `shouldBe` [False]
  describe "findLowercase" $ do
    it "findLowercase []" $
      findLowercase [] `shouldBe` []
  describe "findLowercase " $ do
    it "findLowercase of [Hello, goodbye, Test, test]" $
      findLowercase ["Hello", "goodbye", "Test", "test"] `shouldBe` [False, True,False, True]