-- file test/DailyOneSpec.hs
module DailyOneSpec where

import Test.Hspec 
import DailyOne

spec :: Spec
spec = do
  describe "quadratic" $ do
    it "produces the result of 1 1 1 1" $
      quadratic 1 1 1 1 `shouldBe` 3

    it "produces the result of 1 2 3 4 " $
      quadratic 1 2 3 4 `shouldBe` 57

    it "produces the result of 2 3 4 5" $
      quadratic 2 3 4 5 `shouldBe` 117

    it "produces the result of 18 9 4 5" $
      quadratic 8 9 4 5 `shouldBe` 153


  describe "scaleVector" $ do
    it "Scale Vector 3 (3,4)" $
      scaleVector 3 (3,4) `shouldBe` (9,12)

    it "Scale Vector 4 (1,1)" $
      scaleVector 4 (1,1) `shouldBe` (4,4)

    it "Scale Vector 5 (6,5)" $
      scaleVector 5 (6,5) `shouldBe` (30,25)

    it "Scale Vector 2 (6,5)" $
      scaleVector 2 (6,5) `shouldBe` (12,10)

  describe "tripleDistance" $ do
    it "Distance of (1, 2, 3), (4, 5, 1)" $
      tripleDistance (1,2,3) (4,5,6)`shouldBe` 5.196152422706632
    it "Distance of (1, 1, 1) , (1, 1, 1)" $
      tripleDistance (1, 1, 1) (1, 1, 1)`shouldBe` 0.0

    it "Distance of (37, 83,92),(34,22,44)" $
      tripleDistance (37, 83,92) (34,22,44) `shouldBe` 77.67882594375381

    it "Distance of (100, 43 ,59), (1,2,3)" $
      tripleDistance (100, 43 ,59) (1,2,3) `shouldBe` 120.90492132250036

