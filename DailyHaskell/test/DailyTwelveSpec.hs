{-# OPTIONS_GHC -Wno-type-defaults #-}
module DailyTwelveSpec where

import Test.Hspec 
import DailyTwelve

spec :: Spec
spec = do
    describe "onlyNothing" $ do
        it "outputs onlyNothing (x -> if x < 5 then Nothing else Just x) [1,2,3,4]" $
            onlyNothing (\x -> if x < 5 then Nothing else Just x) [1,2,3,4] `shouldBe` True
        it "outputs onlyNothing (x ->  if x < 2 then Nothing else Just x) [1,2,3,4]" $
            onlyNothing (\x ->  if x < 2 then Nothing else Just x) [1,2,3,4] `shouldBe` False
        it "outputs onlyNothing (x -> x ) []" $
            onlyNothing (\x -> x) [] `shouldBe` True
    
    describe "firstAnswer" $ do
        it "outputs  firstAnswer (x -> if x < 5 then Nothing else Just x) [1,2,3,4]" $
            firstAnswer (\x -> if x < 5 then Nothing else Just x) [1,2,3,4] `shouldBe` Nothing
        it "outputs firstAnswer (x ->  if x < 2 then Nothing else Just x) [1,2,3,4])" $
            firstAnswer (\x ->  if x < 2 then Nothing else Just x) [1,2,3,4] `shouldBe` Just 2
        it "outputs firstAnswer (x -> x ) []" $
            firstAnswer (\x -> if x  then Nothing else Just x) [] `shouldBe` Nothing


    describe "allAnswers" $ do
        it "outputs  firstAnswer (x -> if x < 5 then Nothing else Just x) [1,2,3,4]" $
            allAnswers (\x -> if x < 5 then Nothing else Just [x]) [1,2,3,4] `shouldBe` Nothing
        it "outputs firstAnswer (x ->  if x < 2 then Nothing else Just x) [1,2,3,4])" $
            allAnswers (\x ->  if x > 5 then Nothing else Just [x]) [1,2,3,4] `shouldBe` Just [1,2,3,4]
        it "outputs firstAnswer (x -> x ) []" $
            allAnswers (\x -> if x  then Nothing else Just [x]) [] `shouldBe` Just []

