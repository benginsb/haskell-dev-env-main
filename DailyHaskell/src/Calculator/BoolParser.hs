module Calculator.BoolParser where
--  A Parser for things
--    Is a Function from Strings
--  To Lists of pairs
--    of things and Strings

import Calculator.BoolDefinitions
import Calculator.BoolLexer 

-- parseString 
--   Consume a String (that represents a Bool Calculator program)
--   Produce a ParseTree
parseString :: String -> ParseTree 
parseString str = parseTokens (lexString str)

-- parseTokens
--   Consume a list of tokens
--   Produce a Structure representing the Parse Tree of given list of tokens
parseTokens :: [Token] -> ParseTree 
parseTokens tokens = let (tree, remTokens) = parseExpression tokens 
                        in
                            case remTokens of 
                                [] -> tree 
                                _ -> error $
                                       "Error: tokens remaininng: " ++ show remTokens

-- parseExpression
--   Consume a list of tokens
--   Produce a structure representing the parse tree and the remaining list
--     of tokens
parseExpression :: [Token] -> (ParseTree, [Token])
parseExpression tokens =
    case lookAhead tokens of 
        TokenBoolConst v -> if v == "True"
                               then (ValueNode (BoolType True), (accept tokens))
                               else (ValueNode (BoolType False), (accept tokens))
        TokenName name -> ((IdNode name), (accept tokens))
        -- TokenEqual -> (EqualNode, (accept tokens))
        -- TokenKeyword "In" -> (InNode, (accept tokens))
        TokenAnd ->
            let (firstExpr, tokens') = parseExpression (accept tokens)
               in 
                let (secondExpr, tokens'') = parseExpression tokens'
                  in 
                    ((AndNode firstExpr secondExpr), tokens'')
        TokenOr ->
            let (firstExpr, tokens') = parseExpression (accept tokens)
               in 
                let (secondExpr, tokens'') = parseExpression tokens'
                  in 
                    ((OrNode firstExpr secondExpr), tokens'')
        TokenNot ->
            let (expr, tokens') = parseExpression (accept tokens)
                in
                    (NotNode expr, tokens')
        TokenKeyword "Let" -> 
            let (id, tokens') = parseExpression (accept tokens)
              in 
                let (equalSign, tokens'') = parseExpression tokens'
                   in 
                    let (exprOne, tokens''') = parseExpression tokens''
                      in
                        let (inWord, tokens'''') = parseExpression tokens'''
                          in
                            let (exprTwo, tokens''''') = parseExpression tokens''''
                              in
                                ((LetNode id exprOne exprTwo), tokens''''')
        TokenKeyword "Lambda" ->
            let (id, tokens') = parseExpression (accept tokens)
               in
                let (inWord, tokens'') = parseExpression tokens'
                  in 
                    let (bodyExpr, tokens''') = parseExpression tokens'' 
                      in
                        ((LambdaNode id bodyExpr), tokens''')
        TokenKeyword "Call" ->
            let (id, tokens') = parseExpression (accept tokens)
              in
                let (bodyExpr, tokens'') = parseExpression tokens'
                  in
                    ((CallNode id bodyExpr), tokens'')
        _ -> (EmptyNode, tokens)

-- Helper Functions
-- lookAhead
lookAhead :: [Token] -> Token 
lookAhead [] = TokenEnd 
lookAhead (tok : _) = tok 

-- accept
accept :: [Token] -> [Token]
accept [] = error "No Remaining Token to Accept"
accept (_ : tokens) = tokens