{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module DailyEight where

data Event = Event
  { name :: String
  , day :: Int
  , month :: String
  , year :: Int
  , xlocation :: Double
  , ylocation :: Double
  } deriving (Show, Eq)
    

-- inYear
-- takes in a year and outputs if the year matches any of the years in the evnet
inYear :: Int -> [Event] -> [Event]
inYear _ [] = []
inYear y (x:xs) 
    | y == year x = x : inYear y xs 
    | otherwise = inYear y xs


-- inDayRange
-- takes in two dates and outputs if the dates are in the range of the ones inputed
inDayRange :: Int -> Int -> [Event] -> [Event]
inDayRange _ _ [] = []
inDayRange day1 day2 (x:xs)
    | day x >= day1 && day x <= day2 = x : inDayRange day1 day2 xs
    | otherwise = inDayRange day1 day2 xs

-- inArea
-- Takes in a name and lower and upper x and y and if the x and y is in the range it prints the event
-- name,  a lower x location, an upper x location, a lower y location, an upper y location and a list of event structures and produces a new list
inArea :: String -> Double -> Double -> Double -> Double -> [Event] ->[Event]
inArea _ _ _ _ _ [] = []
inArea n lowerX upperX lowerY upperY (x:xs)
    | n == name x && lowerX <= xlocation x && upperX >= xlocation x && lowerY <= ylocation x && upperY >= ylocation x = x : inArea n lowerX upperX lowerY upperY xs
    | otherwise = inArea n lowerX upperX lowerY upperY xs