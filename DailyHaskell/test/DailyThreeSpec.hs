-- file test/DailyThreeSpec.hs
{-# OPTIONS_GHC -Wno-type-defaults #-}
module DailyThreeSpec where

import Test.Hspec 
import DailyThree

spec :: Spec
spec = do
    describe "removeAllExcept" $ do
        it "outputs removeAllExcept 5 [1,2,3,4,5]" $
            removeAllExcept 5 [1,2,3,4,5] `shouldBe` [5]
        it "outputs removeAllExcept of 'a' ['a','b','b','z','a']" $
            removeAllExcept 'a' ['a','b','b','z','a'] `shouldBe` ['a','a']

    describe "countOccurrences" $ do
        it "outputs countOccurrences number of 5 [1,2,3,4,5]" $
            countOccurrences 5 [1,2,3,4,5] `shouldBe` 1
        it "outputs countOccurrences number of 'a' ['a','b','b','z','a']" $
            countOccurrences 'a' ['a','b','b','z','a'] `shouldBe` 2

    describe "substitute" $ do
        it "outputs substitute number of 4 5 [1,2,3,4,5]" $
            substitute 4 5[1,2,3,4,5] `shouldBe` [1,2,3,5,5]
        it "outputs substitute number of 'a' 'c' ['a','b','b','z','a']" $
            substitute 'a' 'c' ['a','b','b','z','a'] `shouldBe` ['c','b','b','z','c']
    
