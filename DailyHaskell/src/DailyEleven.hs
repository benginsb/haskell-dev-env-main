{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module DailyEleven where
import Data.Either

-- allLefts
-- Uses "lefts" fucntion and returnes all values that are lefts
allLefts :: [Either a b] -> [a]
allLefts [] = []
allLefts (x:xs) = case x of
  Left y -> y : allLefts xs
  Right y -> allLefts xs

-- produceStringOrSum
-- Returnes left if it is the left or rgiht va``lues
    -- else returns the sum or both rights
produceStringOrSum :: (Either String Integer) -> (Either String Integer) -> (Either String Integer)
produceStringOrSum (Left x) _ = Left x
produceStringOrSum _ (Left y) = Left y
produceStringOrSum (Right x) (Right y) = Right (x + y)

-- sumListOfEither
-- If there is any left, return left
-- Else recrusively call over the list adding up all the rights`
    -- Checking if there is any lefts
sumListOfEither :: [Either String Integer] -> (Either String Integer)
sumListOfEither [] = Right 0
sumListOfEither (Left x : _) = Left x
sumListOfEither (Right x : xs) = case sumListOfEither xs of
    Left s -> Left s
    Right add -> Right (x + add)