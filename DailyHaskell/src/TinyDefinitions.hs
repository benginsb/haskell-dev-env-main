module TinyDefinitions where

  data ParseTree = INode |
                   KNode |
                   SNode |
                   IdNode String |
                   ApplyTwoNode ParseTree ParseTree |
                   ApplyThreeNode ParseTree ParseTree ParseTree |
                   ApplyFourNode ParseTree ParseTree ParseTree ParseTree |
                   EmptyNode
                    deriving (Eq,Show)
