module Calculator.BoolEval where

import Calculator.BoolParser ( parseTokens )
import Calculator.BoolLexer (lexString)
import Calculator.BoolDefinitions ( ParseTree(ValueNode, NotNode, AndNode, OrNode, IdNode, LetNode, LambdaNode, CallNode), ValueType(BoolType, ClosureType), ClosureStructure(Closure), EnvType )
import Calculator.Env ( emptyEnv, extendEnv, applyEnv )

-- compileAndRun
--   Consume a String which is the program in the Boolean Calculator Language
--   Produces the result of Lexing, Parsing, and Evaluating the Program
compileAndRun :: String -> ValueType
compileAndRun program = evaluate (parseTokens (lexString program)) emptyEnv

-- evaluate
--   Consume a Parse Tree and an Environment
--   Produce the result value of evaluating the given Parse Tree
evaluate :: ParseTree -> EnvType -> ValueType
evaluate tree env = case tree of 
                  (ValueNode val) -> val
                  (IdNode name) -> (applyEnv name env)
                  (NotNode val) -> let param = evaluate val env
                                      in
                                        case param of 
                                            BoolType True -> BoolType False 
                                            BoolType False -> BoolType True 
                  (AndNode valOne valTwo) -> let paramOne = evaluate valOne env
                                                 paramTwo = evaluate valTwo env
                                                   in 
                                                    case paramOne of
                                                        BoolType True -> case paramTwo of
                                                                            BoolType True -> BoolType True 
                                                                            BoolType False -> BoolType False 
                                                        BoolType False -> BoolType False 
                  (OrNode valOne valTwo) -> let paramOne = evaluate valOne env
                                                paramTwo = evaluate valTwo env
                                                   in 
                                                    case paramOne of
                                                        BoolType True -> BoolType True 
                                                        BoolType False -> case paramTwo of
                                                                            BoolType True -> BoolType True 
                                                                            BoolType False -> BoolType False
                  (LetNode (IdNode name) value body) -> let valResult = evaluate value env
                                                           in
                                                             evaluate body (extendEnv (name, valResult) env)
                  (LambdaNode (IdNode formalParam) body) -> ClosureType (Closure formalParam body env)
                  (CallNode (IdNode funName) expr) -> let result = applyEnv funName env 
                                                        in
                                                          case result of
                                                            ClosureType (Closure paramName functionBody functionEnv) ->
                                                                          (evaluate functionBody 
                                                                                    (extendEnv (paramName, (evaluate expr env)) functionEnv))
                                                            _ -> error "Illegal Function Call"
                                    