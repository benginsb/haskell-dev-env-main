{-# OPTIONS_GHC -Wno-type-defaults #-}
module DailyTenSpec where

import Test.Hspec 
import DailyTen
import Data.Char

spec :: Spec
spec = do
    describe "firstFunctorLaw " $ do
        it "outputs firstFunctorLaw Just ('c', 35)" $
            firstFunctorLaw (Just ('c', 35)) `shouldBe` True
        it "outputs firstFunctorLaw [2, 3, 5, 7, 11]" $
            firstFunctorLaw [2, 3, 5, 7, 11] `shouldBe` True


    describe "secondFunctorLaw " $ do
        it "outputs secondFunctorLaw isAlpha fst (Just('c',35))" $
            secondFunctorLaw isAlpha fst (Just('c',35)) `shouldBe` True
        it "outputs shorterThan 2 [hi,bye,hello,Goodbye]" $
            secondFunctorLaw chr (+ 96)[2,3,5,7,11] `shouldBe` True

    describe "firstFunctorLaw Either " $ do
        it "outputs firstFunctorLaw (Right Nothing :: Either String (Maybe Integer))" $ 
            firstFunctorLaw (Right Nothing :: Either String (Maybe Integer)) `shouldBe` True
        it "outputs firstFunctorLaw (Right (Just 3):: Either String (Maybe Integer))" $ 
            firstFunctorLaw (Right (Just 3):: Either String (Maybe Integer)) `shouldBe` True
        it "outputs firstFunctorLaw (Just 2)" $ 
            firstFunctorLaw (Just 2) `shouldBe` True
    
    describe "secondFunctorLaw Either " $ do
        it "outputs secondFunctorLaw (fmap (+1)) (fmap(*4)) (Right Nothing:: Either String (Maybe Integer))" $ 
            secondFunctorLaw (fmap (+1)) (fmap(*4)) (Right Nothing:: Either String (Maybe Integer)) `shouldBe` True
        it "outputs secondFunctorLaw (fmap(*2)) (fmap(+3)) (Right (Just 3):: Either String (Maybe Integer))" $ 
            secondFunctorLaw (fmap(*2)) (fmap(+3)) (Right (Just 3):: Either String (Maybe Integer)) `shouldBe` True
        it "outputs  secondFunctorLaw  (/2) (*9) (Just 2)" $ 
            secondFunctorLaw  (/2) (*9) (Just 2) `shouldBe` True

