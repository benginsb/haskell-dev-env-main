module MiniRacketParser where

import Parser
import Expr
import Control.Applicative
import Error ( ErrorT )



parseBool :: Parser Bool
parseBool = do
        parseKeyword "true"
        return True
        <|> do
        parseKeyword "false"
        return False

-- implement parsing bool operations, these are 'and' and 'or'
parseBoolOp :: Parser BoolOp
parseBoolOp = do symbol "and" >> return And
              <|>
              do symbol "or" >> return Or



-- parse math operations and return the MathOp
-- TODO: Add the other math operations: *, div, mod
parseMathOp :: Parser MathOp
parseMathOp =
        do symbol "+" >> return Add
    <|> do symbol "-" >> return Sub
    <|> do symbol "*" >> return Mul
    <|> do symbol "/" >> return Div
    <|> do symbol "%" >> return Mod


-- parse the comp operations and return the CompOp
-- TODO: add the comparison operators: equals?, < 
parseCompOp :: Parser CompOp
parseCompOp = do symbol "<" >> return Lt
          <|> do symbol "equals?" >> return Eq

-- a literal in MiniRacket is true, false, or a number
-- TODO: parse literals which can be natural numbers or bools (true, false)
literal :: Parser Value
literal = do
            IntVal <$> natural
            <|>
            BoolVal <$> parseBool


-- parse a literal expression, which at this point, is just a literal
literalExpr :: Parser Expr
literalExpr = do
    LiteralExpr <$> literal


keywordList :: [String]
keywordList = ["false", "true", "not", "and", "or", "equal?"]

-- try to parse a keyword, otherwise it's a variable, this can be
-- used to check if the identifier we see (i.e., variable name) is
-- actually a keyword, which isn't legal
parseKeyword :: String -> Parser String
parseKeyword keyword = do
    -- all keywords follow the identifier rules, so we'll use that
    name <- identifier
    if name `elem` keywordList && keyword == name
    then return name
    else failParse $ "saw " ++ name ++ ", expected " ++ keyword


-- TODO: parse not expressions, note that "not" is a keyword, so
-- as a big hint, you should use parseKeyword
notExpr :: Parser Expr
notExpr = do
          parseKeyword "not"
          parseExpr

{- DON'T DEFINE THESE YET, THEY'RE NOT PART OF THE ASSIGNMENT 
-- varExpr :: Parser Expr
-- negateExpr :: Parser Expr
-}

-- a bool expression is the operator followed by one or more expressions that we have to parse
-- TODO: add bool expressions 
boolExpr :: Parser Expr
boolExpr = do
           first <- parseBoolOp
           expression <- kplus parseExpr
           return (BoolExpr first expression)


-- a math expression is the operator followed by one or more expressions that we have to parse
-- TODO: add math expressions
mathExpr :: Parser Expr
mathExpr = do
           first <- parseMathOp
           expression <- kplus parseExpr
           return (MathExpr first expression)


-- a comp expression is the comp operator and the parsing of two expressions
compExpr :: Parser Expr
compExpr = CompExpr <$> parseCompOp <*> parseExpr <*> parseExpr

{- DON'T DEFINE THESE YET, THEY'RE NOT PART OF THE ASSIGNMENT 
-- ifExpr :: Parser Expr
-- applyExpr :: Parser Expr
-- letExpr :: Parser Expr
-}

pairExpr :: Parser Expr
pairExpr = do
    expression <- parseExpr
    symbol "."
    PairExpr expression <$> parseExpr

-- note that this is syntactic sugar, cons is just replaced by the PairExpr ast
consExpr :: Parser Expr
consExpr = do
    symbol "cons"
    expression <- parseExpr
    PairExpr expression <$> parseExpr



parseParens :: Parser Expr -> Parser Expr
parseParens p = do
    symbol "("
    e <- p
    symbol ")"
    return e

{- DON'T DEFINE THESE YET, THEY'RE NOT PART OF THE ASSIGNMENT 
-- negateAtom :: Parser Expr
-- lambdaExpr :: Parser Expr
-}

-- an atom is a literalExpr, which can be an actual literal or some other things
parseAtom :: Parser Expr
parseAtom = do
    literalExpr


parseExpr :: Parser Expr
parseExpr = do
    parseAtom
    <|> parseParens notExpr
    <|> parseParens boolExpr
    <|> parseParens mathExpr
    <|> parseParens ifExpr
    <|> parseParens applyExpr
    <|> parseParens letExpr
    <|> parseParens lambdaExpr
    <|> parseParens parseExpr
    <|> parseParens compExpr
    <|> parseParens pairExpr
    <|> parseParens consExpr

 -- a helper function that you can use to test your parsing:
-- syntax is simply 'parseStr "5"' which will call parseExpr for you
parseStr :: String -> Either ErrorT (Expr, String)
parseStr str = do
    parse parseExpr str

-- the main parsing function which alternates between all the options you have
-- parseExpr :: Parser Expr
-- parseExpr = do
--     parseAtom
--     <|> parseParens notExpr
--     <|> parseParens boolExpr
--     <|> parseParens mathExpr
--     <|> parseParens compExpr
--     <|> parseParens pairExpr
--     <|> parseParens consExpr
--     <|> parseParens parseExpr



-- TODO: Implement negateAtom
-- negate an atom, we actually only have one choice here. Our
-- parsing already correctly builds negative numbers, and we
-- can't have negative boolean values (so we won't bother parsing)
-- those. That leaves variables, but this needs to build a 
-- NegateExpr around the VarExpr.
negateAtom :: Parser Expr
negateAtom = do
    failParse "Not implemented"





-- TODOmplement varExpr
-- parse a var expression, here we need to make sure that
-- the identifier is *not* a keyword before accepting it
-- i.e., we fail the parse if it is     
varExpr :: Parser Expr
varExpr = do 
    failParse "Not implemented"

-- TODO: Implement ifExpr
-- parse an if-expression, which begins with the keyword if,
-- and is followed by three expressions
ifExpr :: Parser Expr
ifExpr = do
  parseKeyword "if"
  boolexpr  <- parseExpr
  texpr <- parseExpr
  IfExpr boolexpr texpr <$> parseExpr



-- TODO: Implement let expressions  
-- a let expression begins with the keyword let, followed by
-- parenthesis which contains an identifier for the name 
-- to be bound, an expression to bind to that name, a close
-- parenthesis, and a body  
letExpr :: Parser Expr
letExpr = do
    parseKeyword "let"
    symbol "("
    e <- identifier 
    symbol ")"
    expression <- parseExpr
    LetExpr e expression <$> parseExpr

-- TODO: Implement lambdaExpr 
-- parse a lambda expression which is a lambda, argument, 
-- and body, with proper parenthesis around it
lambdaExpr :: Parser Expr
lambdaExpr = do
    parseKeyword "lambda"
    symbol "("
    argument <- identifier
    symbol ")"
    LambdaExpr argument <$> parseExpr

--TODO: Implement applyExpr
-- what we do know is that the left argument will result in a function,
-- otherwise we'll have an error, but nesting them like this allows us
-- to further build up functions
applyExpr :: Parser Expr
applyExpr = do
    failParse "Not implemented"

--TODO: Add 
-- the main parsing function which alternates between all 
-- the options you have for possible expressions