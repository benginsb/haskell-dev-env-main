{-# OPTIONS_GHC -Wno-type-defaults #-}
module DailySixSpec where

import Test.Hspec 
import DailySix

spec :: Spec
spec = do
    describe "shorterThan" $ do
        it "outputs shorterThan 5 [apple,bananas,pear,watermellon]" $
            shorterThan 5 ["apple","bananas","pear","watermellon"] `shouldBe` ["apple","pear"]
        it "outputs shorterThan 2 [hi,bye,hello,Goodbye]" $
            shorterThan 3 ["hi","bye","hello","Goodbye"] `shouldBe` ["hi","bye"]
        it "outputs shorterThan 5 []" $
            shorterThan 5 [] `shouldBe` []

    describe "removeMultiples" $ do
        it "outputs removeMultiples number of 5 [1,2,3,4,5]" $
            removeMultiples 5 [1,2,3,4,5] `shouldBe` [1,2,3,4]
        it "outputs removeMultiples number of 1 []" $
            removeMultiples 1 [] `shouldBe` []
        it "outputs removeMultiples number of  5 [3,5,10,9, 15] " $
            removeMultiples  5 [3,5,10,9, 15]  `shouldBe` [3,9]


    describe "onlyJust" $ do
        it "outputs onlyJuste number of onlyJust [Nothing, Just 5, Nothing, Just 10]" $
            onlyJust [Nothing, Just 5, Nothing, Just 10] `shouldBe` [Just 5, Just 10]
        it "outputs onlyJust number of [Just 15]" $
            onlyJust [Just 15] `shouldBe` [Just 15]
        it "outputs onlyJust number of onlyJust [Just 10, Nothing, Just 5, Nothing, Just 12, Nothing, Just 10]" $
            onlyJust [Just 10, Nothing, Just 5, Nothing, Just 12, Nothing, Just 10] `shouldBe` [Just 10, Just 5, Just 12, Just 10]
