module ExamplesDayFive where
quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = let (smaller, larger) = split x xs
                        in
                            quicksort smaller ++ [x] ++ quicksort larger 


    -- split for Quicksort
        -- Consume a pivot (splitting element) and a List of elements
        -- Produce a tuple with two subits: all of the elments smaller than the pivot
            -- and all the elements larger than the pivot
split :: (Ord a) => a -> [a] -> ([a], [a])
split _ [] = ([],[])
split p (x:xs) = let (first,second) = split p xs
                        in
                            if x < p
                                then (x: first, second)
                                else (first, x : second)


split' :: (Ord a) => a -> [a] -> ([a],[a])
split' _ [] = ([],[])
split' p elems = ([e | e <- elems, e <= p], [f | f <- elems, f > p])    


-- splitM
-- consume a list
-- Produce a tupe with 2 lists of half the size
splitM :: [a] -> ([a],[a])
splitM [] = ([],[])
splitM [e] = ([e], [])
splitM(e:f:elems) = let (first,second) = splitM elems
                        in
                            (e:first, f:second)

splitM' :: [a] -> ([a],[a])
splitM' [] = ([],[])
splitM' theList = let num = length theList `div` 2
                        in
                            (take num theList, drop num theList)


mergesort ::(Ord a) => [a] -> [a]
mergesort [] = []
mergesort theList = let (firstHalf, secondHalf) = splitM theList
                        in
                            merge (mergesort firstHalf) (mergesort secondHalf)


merge :: (Ord a) => [a] -> [a] -> [a]
merge [] [] = []
merge (x:xs) [] = x:xs
merge [] (x:xs) = x:xs
merge (x:xs) (y:ys) = if x < y
                        then x: (merge xs (y:ys))
                        else y: (merge (x:xs) ys)
