
module WeeklyThree where

-- creatomg a new typeclass, Vec
    data Vec = Vec [Double]
        deriving (Show)

    -- Vec as a member of Num
    -- FIlls in the prompts with       
    --   (+) = _
    --   (*) = _
    --   abs = _
    --   signum = _
    --   fromInteger = _
    --   negate = _
    instance Num Vec where
        (+) (Vec x) (Vec y) = Vec (zipWith (+) x y)
        (-)(Vec x) (Vec y) = Vec (zipWith (-) x y)
        (*)(Vec x) (Vec y) = Vec (zipWith (*) x y)
        abs (Vec x) = Vec (map abs x)
        signum (Vec x) = Vec (map signum x)
        fromInteger n = Vec [fromInteger n]   
        negate (Vec x) = Vec (map negate x)

    -- Vec as a member of Eq
    -- (==) (Vec x) (Vec y)
    instance Eq Vec where
      (==) (Vec x) (Vec y) = and (zipWith (==) x y)
    
    -- instantiate Ord
    -- (Vec x) `compare` (Vec y)
    instance Ord Vec where    
      (>) (Vec x) (Vec y) =  x > y
      (<) (Vec x) (Vec y) =  x < y
      (>=) (Vec x) (Vec y) =  x >= y
      (<=) (Vec x) (Vec y) =  x <= y

    -- Create a new typeclass called VecT
    class VecT a where
        magnitude :: a -> Double         
 
    -- instantiate Vec as a VecT
    -- magnitude (Vec x)
    -- FIXED
    instance VecT Vec where
        magnitude (Vec x) = sqrt (sum (map (^2) x))

    -- Instantiate Vec as a Semigroup
    -- (Vec x) <> (Vec y)
    instance Semigroup Vec where
        (Vec x) <> (Vec y) = Vec (zipWith (+) x y)

    -- Instantiate Vec as a Monoid
    -- mempty
    instance Monoid Vec where
        mempty = Vec (repeat 0)