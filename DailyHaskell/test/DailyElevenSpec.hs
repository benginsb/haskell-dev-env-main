{-# OPTIONS_GHC -Wno-type-defaults #-}
module DailyElevenSpec where

import DailyEleven
import Test.Hspec 



spec :: Spec
spec = do
    describe "allLefts " $ do
        it "outputsallLefts [Left a, Right 3, Left c]" $
            allLefts [Left "a", Right 3, Left "c"] `shouldBe` allLefts [Left "a", Left"c"]
        it "outputs allLefts [Left a , Right 1, Left b, Right 2, Left c, Right 3]" $
            allLefts [Left "a" , Right 1, Left "b", Right 2,Left "c", Right 3] `shouldBe` allLefts [ Left "a", Left "b", Left "c"]
        it "outputs allLefts [Left a]" $
            allLefts [Left "a"] `shouldBe` allLefts [Left "a"]

    describe "allLefts " $ do
        it "outputs produceStringOrSum (Right 10) (Left a)" $
            produceStringOrSum (Right 10) (Left "a") `shouldBe` Left "a"
        it "outputs produceStringOrSum (Left b) (Right 11)" $
            produceStringOrSum (Left "b") (Right 11) `shouldBe` Left "b"
        it "outputs produceStringOrSum (Right 1) (Right 2)" $
            produceStringOrSum (Right 1) (Right 2) `shouldBe` Right 3

    describe "sumListOfEither " $ do
        it "outputs sumListOfEither [Right 10, Right 11, Right 12]" $
            sumListOfEither [Right 10, Right 11, Right 12] `shouldBe` Right 33
        it "outputs sumListOfEither [Right 1, Left b, Right 2, Left c, Right 3]" $
            sumListOfEither [Right 1, Left "b", Right 2, Left "c", Right 3] `shouldBe` Left "b"
        it "outputs allLefts [Right 1]" $
            sumListOfEither [Right 1] `shouldBe` Right 1
