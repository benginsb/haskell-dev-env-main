module TinyEvaluator where

import TinyParser
import TinyLexer

import TinyDefinitions

-- compileAndRun
--   Consumes a String which is the program
--   Produces the result of lexing, parsing, and evaluating the program
compileAndRun :: String -> ParseTree
compileAndRun program = evaluate (parseString program)

-- evaluate
--   Consume a Parse Tree
--   Produce the result value of evaluating the given Parse Tree
evaluate :: ParseTree -> ParseTree
evaluate tree = case tree of
                      (INode) -> INode
                      (KNode) -> KNode 
                      (SNode) -> SNode 
                      (IdNode var) -> IdNode var
                      (ApplyTwoNode f p) -> let fval = evaluate f
                                                v = evaluate p 
                                              in
                                                case fval of
                                                  INode -> v 
                                                  _ -> ApplyTwoNode fval v
                      (ApplyThreeNode f p1 p2) -> let fval = evaluate f
                                                      v1 = evaluate p1 
                                                      v2 = evaluate p2
                                                    in
                                                      case fval of
                                                        (INode) -> ApplyTwoNode v1 v2
                                                        (KNode) -> v1 
                                                        _ -> ApplyThreeNode fval v1 v2
                      (ApplyFourNode f p1 p2 p3) -> let fval = evaluate f
                                                        v1 = evaluate p1 
                                                        v2 = evaluate p2 
                                                        v3 = evaluate p3 
                                                      in
                                                        case fval of 
                                                          (INode) -> ApplyThreeNode v1 v2 v3
                                                          (KNode) -> ApplyTwoNode v1 v3
                                                          (SNode) -> ApplyThreeNode v1 
                                                                                     v3
                                                                                     (ApplyTwoNode v2 v3)
                                                          _       -> ApplyFourNode fval v1 v2 v3
                      EmptyNode -> EmptyNode
--                      (NotNode val) -> let param = (evaluate val env)
--                                       in 
--                                            case param of 
--                                               (BoolType True) -> (BoolType False)
--                                               (BoolType False) -> (BoolType True)
 
