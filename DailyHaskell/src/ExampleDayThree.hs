-- file src/ExampleDayThree.hs
module ExampleDayThree where
    -- first of a tuple
    -- Consume a 2-tuple
    -- produce the value of the first element of the tuple
    first :: (a, b) -> a
    first tup = fst tup

    -- first of a tuple
    -- Consume a 2-tuple
    -- produce the value of the first element of the tuple
    f :: (a,b) -> a
    f (x,y) = x

    dotProduct :: (Integer, Integer) -> (Integer, Integer) -> Integer
    dotProduct (x1,y1) (x2,y2) = (x1 * x2) + (y1  * y2)

    len :: [a] -> Integer 
    len [] = 0
    len (e: es) = 1 + len es

    addTheList :: [Integer] -> Integer 
    addTheList [] = 0
    addTheList (x:xs) = x + addTheList xs

    addNumToList :: Integer -> [Integer] -> [Integer]
    addNumToList n [] = []
    addNumToList n (x:xs) = (n + x) : (addNumToList n xs)
