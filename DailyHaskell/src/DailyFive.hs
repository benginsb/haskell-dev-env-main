{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module DailyFive where
import Data.Char (isLower)

-- multPairs
-- Take the map of the multiplaction
multPairs :: [(Integer ,Integer)] -> [Integer]
multPairs [] = []
multPairs xs = map (\(x, y) -> x * y) xs

-- squareList
-- Takes the map of a number squared
squareList :: [Integer] -> [(Integer ,Integer)]
squareList [] = []
squareList xs = map (\ x -> (x,x *x)) xs

-- findlowercase
-- Takes the map of a frist letter then applies the isLower funciton
findLowercase :: [String] -> [Bool]
findLowercase [] = []
findLowercase xs = map (\x -> isLower (head x)) xs