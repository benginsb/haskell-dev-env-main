module DailyEightSpec where


import Test.Hspec 
import DailyEight




spec :: Spec
spec = do
  describe "inYear" $ do
    let event = [Event "Event 1" 1 "January" 2022 14 23,
                 Event "Event 2" 2 "October" 2019 1.1 2.2,
                 Event "Event 3" 3 "May" 2090 123 321,
                 Event "Event 4" 12 "September" 2002 0.123456789 10.98765432,
                 Event "Event 5" 2 "August" 2022 12.321 321.12]
    it "inYear 2023 event" $ do
      inYear 2023 event `shouldBe` []
    it "inYear 2090 event" $ do
      inYear 2090 event `shouldBe` [Event "Event 3" 3 "May" 2090 123 321]
    it "inYear 2022 event" $ do
      inYear 2022 event `shouldBe` [Event {name = "Event 1", day = 1, month = "January", year = 2022, xlocation = 14.0, ylocation = 23.0},Event {name = "Event 5", day = 2, month = "August", year = 2022, xlocation = 12.321, ylocation = 321.12}]


    describe "inDayRange" $ do
      it "inDayRange 1 2 event" $ do
        inDayRange 1 2 event `shouldBe`  [Event {name = "Event 1", day = 1, month = "January", year = 2022, xlocation = 14.0, ylocation = 23.0},Event {name = "Event 2", day = 2, month = "October", year = 2019, xlocation = 1.1, ylocation = 2.2},Event {name = "Event 5", day = 2, month = "August", year = 2022, xlocation = 12.321, ylocation = 321.12}]
      it "inDayRange 3 123 event" $ do
        inDayRange 3 12 event `shouldBe` [Event "Event 3" 3 "May" 2090 123 321,Event "Event 4" 12 "September" 2002 0.123456789 10.98765432]
      it "inDayRange 13 24 event" $ do
        inDayRange 13 24 event `shouldBe` []

    describe "inArea" $ do
      it "inArea Event 2 0 2 2 3" $ do
        inArea "Event 2" 0 2 2 3 event `shouldBe` [Event {name = "Event 2", day = 2, month = "October", year = 2019, xlocation = 1.1, ylocation = 2.2}]
      it "inArea Event 1 0 2 2 3" $ do
        inArea "Event 1" 0 2 2 3 event `shouldBe` []
      it "inArea Event 4 20 2 35" $ do
        inArea "Event 4" 0 20 2 35 event `shouldBe` [Event {name = "Event 4", day = 12, month = "September", year = 2002, xlocation = 0.123456789, ylocation = 10.98765432}]
