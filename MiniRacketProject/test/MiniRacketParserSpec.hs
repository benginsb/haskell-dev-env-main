module MiniRacketParserSpec where 

import Test.Hspec
import Parser
import Expr 
import MiniRacketParser
import Error

type ParseResult = Either ErrorT (Expr, String)

expr :: Either ErrorT (a2, b) -> a2
expr (Right (e, _)) = e 
expr (Left (SyntaxError msg)) = error msg
expr (Left (ParseError msg)) = error msg
expr (Left NoParse) = error "no matching parse"
expr _ = error "expr in MiniRacketParser.hs is not fully implemented yet..."

spec :: Spec 
spec = do 
    describe "parse literals" $ do
        it "parses number: 1235" $ 
            parseStr "1235" `shouldBe` Right (LiteralExpr (IntVal 1235),"")
        it "parses negative numbers: -12235" $
            parseStr "-12235" `shouldBe` Right (LiteralExpr (IntVal (-12235)), "")
        it "parses true" $
            parseStr "true" `shouldBe` Right (LiteralExpr (BoolVal True), "")
        it "parses false" $
            parseStr "false" `shouldBe` Right (LiteralExpr (BoolVal False), "")
        it "parses number: 5" $ 
            parseStr "5" `shouldBe` Right (LiteralExpr (IntVal 5),"")    
        it "parses true" $ 
            parseStr "true" `shouldBe` Right (LiteralExpr (BoolVal True),"")    
        it "parses and" $ 
            parse parseBoolOp "and" `shouldBe` Right (And, "")  
        it "parses or" $ 
            parse parseBoolOp "or" `shouldBe` Right (Or, "")
        it "parses not false" $ 
            parse notExpr "not false" `shouldBe` Right (LiteralExpr (BoolVal False),"")
        it "parses not true" $ 
            parse notExpr "not true" `shouldBe` Right (LiteralExpr (BoolVal True),"")   
        it "parses * (+ 1 2) (- 3 4)" $ 
            parse mathExpr  "* (+ 1 2) (- 3 4)" `shouldBe` Right (MathExpr Mul [MathExpr Add [LiteralExpr (IntVal 1),LiteralExpr (IntVal 2)],MathExpr Sub [LiteralExpr (IntVal 3),LiteralExpr (IntVal 4)]],"")
        it "parses and false (and true true true)" $ 
            parse boolExpr "and false (and true true true)" `shouldBe` Right (BoolExpr And [LiteralExpr (BoolVal False),BoolExpr And [LiteralExpr (BoolVal True),LiteralExpr (BoolVal True),LiteralExpr (BoolVal True)]],"")
        it "parse boolExpr and true (or true false)" $
            parse boolExpr "and true (or true false)" `shouldBe` Right (BoolExpr And [LiteralExpr (BoolVal True),BoolExpr Or [LiteralExpr (BoolVal True),LiteralExpr (BoolVal False)]],"")
        it "parse mathExpr  / (* 10 11) (+ 8 9)" $
            parse boolExpr "and true (or true false)" `shouldBe` Right (MathExpr Div [MathExpr Mul [LiteralExpr (IntVal 10),LiteralExpr (IntVal 11)],MathExpr Add [LiteralExpr (IntVal 8),LiteralExpr (IntVal 9)]],"")



   