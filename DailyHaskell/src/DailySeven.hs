{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module DailySeven where


-- findLongest
-- folding right to evaluate the first word
-- Only return the first string that is longer than the total amount of items in the list
findLongest :: [String] -> String
findLongest [] = ""
findLongest list = foldr (\x acc -> if length x > length list then x else acc) "" list


-- anyLarger
-- foldr though the list and returns true if any number is bigger then num
anyLarger :: Integer -> [Integer] -> Bool 
anyLarger _ [] = False
anyLarger num list = foldr (\x acc -> if x >= num then True else acc) False list

-- allNames
-- for each elemeent in the list, do a foldr to concat
-- if not end, then adds comma, else no comma
allNames :: [(String,String)] -> String
allNames [] = ""
allNames list = foldr (\(x, y) acc -> x ++ " " ++ y ++ (if null acc then "" else ", " ++ acc)) "" list

